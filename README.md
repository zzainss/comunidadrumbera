# README # 

Tema hijo de wordpress basado en la plantilla **Magzimus** 

### ¿Qué es este repositorio para? ### 

* Tema desarrollado para ComunidadRumbera.net

### ¿Cómo puedo configurar? ### 

* Copiar en el directorio ./wp-content/themes/
* Ingresar a ./wp-admin/themes.php
* activar el tema **ComunidadRumbera**

### Cambios al tema ### 

* Modificacion del color y tamaño del menu.

